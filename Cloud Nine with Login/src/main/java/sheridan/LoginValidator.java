package sheridan;

/***
 * 
 * @author harmeet kaur 991539912
 *
 */

public class LoginValidator {

	public static final String REGEX = "^[a-zA-Z][a-zA-Z0-9]{5,}$";
	
	public static boolean isValidLoginName( String loginName ) {
		return loginName.matches(REGEX);
	}
}
