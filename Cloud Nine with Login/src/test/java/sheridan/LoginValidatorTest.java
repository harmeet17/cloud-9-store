package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	//Login name must have alpha characters and numbers only, (no special characters) but 
	//it should not start with a number
	
	@Test
	public void testIsValidLoginRegular( ) {
		String name="harmeet17";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertTrue("The login name entered is valid",isValid);
	}
	
	@Test
	public void testIsValidLoginException( ) {
		String name=" har";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertFalse("The login name entered is not valid",isValid);
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		String name="harmeet1";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertTrue("The login name entered is valid",isValid);
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		String name="1armeet1";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertFalse("The login name entered is not valid",isValid);
	}
	
	//Login name must have at least 6 alphanumeric characters.
	
	@Test
	public void testIsValidLoginAlphaNumberRegular( ) {
		String name="harmeet1";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertTrue("The login name entered is valid",isValid);
	}
	
	@Test
	public void testIsValidLoginAlphaNumberException( ) {
		String name="";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertFalse("The login name entered is not valid",isValid);
	}

	@Test
	public void testIsValidLoginAlphaNumberBoundaryIn( ) {
		String name="harmee1";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertTrue("The login name entered is valid",isValid);
	}
	
	@Test
	public void testIsValidLoginAlphaNumberBoundaryOut( ) {
		String name="harm1";
		boolean isValid = LoginValidator.isValidLoginName(name);
		assertFalse("The login name entered is not valid",isValid);
	}
	
	
}
